<?php

namespace Ratespecial\SpyCloud\Enums;

/**
 * For use with breach catalogs
 *
 * @see https://spyclouddataschema.docs.apiary.io/#reference/confidence
 * @see \Ratespecial\SpyCloud\Models\BreachCatalog
 */
enum Confidence: int
{
    /**
     * This record is part of an email-only list.
     */
    case HIGH = 1;
    /**
     * This severity value is given to breach records where we have a non-crackable password hash, or no password at all.
     */
    case CONFIDENT = 2;
    /**
     * This severity value is given to breach records where we have an email address and a plaintext password.
     */
    case UNVERIFIED = 3;
}
