<?php

namespace Ratespecial\SpyCloud\Enums;

/**
 * For use with BreachRecords
 *
 * @see https://spyclouddataschema.docs.apiary.io/#reference/severity
 * @see \Ratespecial\SpyCloud\Models\BreachRecord
 */
enum Severity: int
{
    /**
     * This record is part of an email-only list.
     */
    case EMAIL_ONLY = 2;
    /**
     * This severity value is given to breach records where we have a non-crackable password hash, or no password at all.
     */
    case INFORMATIONAL = 5;
    /**
     * This severity value is given to breach records where we have an email address and a plaintext password.
     */
    case HIGH = 20;
    /**
     * This severity value is given to breach records recovered from an infected machine (botnet data). These records will always have
     * a plaintext password and most will have an email address.
     * This means malware was installed on a machine and passwords were stolen.
     */
    case CRITICAL = 25;
}
