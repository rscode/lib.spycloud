<?php

namespace Ratespecial\SpyCloud\Enums;

/**
 * For use with getDomainBreachData
 *
 * @see \Ratespecial\SpyCloud\SpyCloudService
 */
enum DomainRecordType: string
{
    case ALL = '';
    case CORPORATE = 'corporate';
    case INFECTED = 'infected';
}
