<?php

namespace Ratespecial\SpyCloud\Models;

use DateTime;
use Exception;
use JsonSerializable;
use Ratespecial\SpyCloud\Enums\Severity;
use Ratespecial\SpyCloud\Models\BreachAssets\AccountAssets;
use Ratespecial\SpyCloud\Models\BreachAssets\CompanyAndJobAssets;
use Ratespecial\SpyCloud\Models\BreachAssets\CrmAssets;
use Ratespecial\SpyCloud\Models\BreachAssets\FinancialAssets;
use Ratespecial\SpyCloud\Models\BreachAssets\InfectedUserAssets;
use Ratespecial\SpyCloud\Models\BreachAssets\MedicalAssets;
use Ratespecial\SpyCloud\Models\BreachAssets\OtherAssets;
use Ratespecial\SpyCloud\Models\BreachAssets\PiiAssets;
use Ratespecial\SpyCloud\Models\BreachAssets\SocialAssets;

/**
 * A record within a breach.  When searching for a criteria like email, phone, or IP this will be the record type.
 *
 * @see https://spyclouddataschema.docs.apiary.io/#introduction/data-schema/breach-records
 */
class BreachRecord implements JsonSerializable
{
    use AccountAssets;
    use CompanyAndJobAssets;
    use CrmAssets;
    use FinancialAssets;
    use InfectedUserAssets;
    use MedicalAssets;
    use OtherAssets;
    use PiiAssets;
    use SocialAssets;

    /**
     * Internal unique identifier for a breach record.
     *
     * @var string
     * @required
     */
    public string $document_id = '';
    /**
     * Numerical breach ID. This correlates directly with the id field in Breach Catalog objects.
     *
     * @var int
     * @required
     */
    public int $source_id = 0;
    /**
     * The date on which this record was ingested into SpyCloud's system.  This correlates with spycloud_publish_date field in
     * Breach Catalog objects.
     *
     * @var DateTime
     * @required
     */
    public DateTime $spycloud_publish_date;
    /**
     * Severity is a numeric code representing severity of a breach record. This can be used in API requests to ensure only Breach Records
     * with plaintext password are returned.
     *
     * @var Severity
     * @required
     */
    public Severity $severity;
    /**
     * Provides an identifier that signifies the unique snapshot of data stolen by malware. Each time a malware infostealer runs and
     * collects data, a new unique log_id is generated. This value can be used to consistently connect records that were gathered within a
     * single run of a malware info-stealer.
     * log_id is intended to replace, or be used jointly, with the infected_machine_id field, which provides a value extracted from the
     * malware record. In practice, infected_machine_id is usually unique, but is not always unique.
     * Only relates to record with severity 25
     *
     * @var string
     */
    public string $log_id = '';


    /**
     * @throws Exception
     */
    public function setSpycloudPublishDate(string $val): self
    {
        $this->spycloud_publish_date = new DateTime($val);

        return $this;
    }


    public function setSeverity(int $val): self
    {
        $this->severity = Severity::from($val);

        return $this;
    }


    public function toArray(): array
    {
        return array_filter(get_object_vars($this));
    }


    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}
