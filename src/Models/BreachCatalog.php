<?php

namespace Ratespecial\SpyCloud\Models;

use DateTime;
use Ratespecial\SpyCloud\Enums\Confidence;

/**
 * A collection of breach records SpyCloud has found.  This contains info on where the discovered the data set and when.
 *
 * @see https://spyclouddataschema.docs.apiary.io/#introduction/data-schema/breach-catalog
 */
class BreachCatalog
{
    /**
     * Numerical breach ID. This number correlates to source_id data point found in breach records.
     * @var int
     * @required true
     */
    public int $id;

    /**
     * UUID v4 encoded version of breach ID. This is relevant for users of Firehose, where each deliverable
     * (records file) is named using the breach UUID.
     * @var string
     * @required true
     */
    public string $uuid;

    /**
     * Breach title. For each ingested breach our security research team documents a breach title. This is only
     * available when we can disclose the breach details, otherwise it will have a generic title.
     * @var string
     * @required true
     */
    public string $title;

    /**
     * Breach description. For each ingested breach our security research team documents a breach description. This is
     * only available when we can disclose the breach details, otherwise it will have a generic description.
     * @var string
     * @required true
     */
    public string $description;

    /**
     * Website of breached organization, when available.
     *
     * @var string
     */
    public string $site = '';

    /**
     * Description of the breached organization, when available.
     * @var string|null
     * @required false
     */
    public ?string $site_description = null;

    /**
     * Denotes if a breach is considered public or private. A public breach is one that is easily found on the internet,
     * while a private breach is often exclusive to SpyCloud.
     * @var string
     * @required true
     */
    public string $type;

    /**
     * Number of records we parsed and ingested from this particular breach. This is after parsing, normalization and
     * deduplication take place.
     * @var int
     * @required true
     */
    public int $num_records;

    /**
     * The date on which we ingested the breached data into our systems. This is the same date on which the data becomes
     * publicly available to our customers.
     * @var DateTime
     * @required true
     */
    public DateTime $spycloud_publish_date;

    /**
     * The date on which our security research team first acquired the breached data.
     *
     * @var DateTime
     */
    public DateTime $acquisition_date;

    /**
     * The date on which we believe the breach took place.
     *
     * @var DateTime|null
     */
    public ?DateTime $breach_date = null;

    /**
     * The date on which this breach was made known to the public. This is usually accompanied by media URLs in _media_urls_ list below.
     *
     * @var DateTime|null
     */
    public ?DateTime $public_date = null;

    /**
     * Array field. List of one or more media URLs referencing the breach in media.
     * @var array|null
     * @required false
     */
    public ?array $media_urls = null;

    /**
     * Dictionary field. A mapping of assets to count for this particular breach.
     * @var array
     * @required true
     */
    public array $assets;

    /**
     * @var Confidence
     * @required
     */
    public Confidence $confidence;

    /**
     * Indicates if the breach is a combo list.
     *
     * @var string
     */
    public string $combo_list_flag = '';

    /**
     * Categorizes breach into combolist, breach, or malware.
     * @var string
     * @required true
     */
    public string $breach_main_category;

    /**
     * Categorizes how the data was breached.
     * @var string
     * @required true
     */
    public string $breach_category;

    /**
     * Companies that are allegedly or confirmed to be involved in the data breach.
     * @var array|null
     * @required false
     */
    public ?array $breached_companies = null;

    /**
     * Companies determined to be targeted by the breach.
     * @var array|null
     * @required false
     */
    public ?array $targeted_companies = null;

    /**
     * General industries determined to be targeted by the breach.
     * @var array|null
     * @required false
     */
    public ?array $targeted_industries = null;

    /**
     * Indicates whether a breach source is sensitive or not.
     * @var bool
     * @required true
     */
    public bool $sensitive_source;

    /**
     * Contains the malware family variant of the infostealer. Only optionally present when breach_category is
     * infostealer.
     * @var string|null
     * @required false
     */
    public ?string $malware_family = null;

    /**
     * Categorization for consumer product mapping.
     * @var string
     * @required true
     */
    public string $consumer_category;

    /**
     * Traffic light protocol. TLP is a set of designations used to ensure that sensitive information is shared with the
     * appropriate audience.
     * @var string
     * @required true
     */
    public string $tlp;

    /**
     * Shortened version of title field when necessary.
     * @var string
     * @required true
     */
    public string $short_title;


    public function setSpycloudPublishDate(string $val): self
    {
        $this->spycloud_publish_date = new DateTime($val);

        return $this;
    }

    public function setAcquisitionDate(string $val): self
    {
        $this->acquisition_date = new DateTime($val);

        return $this;
    }

    public function setBreachDate(string $val): self
    {
        $this->breach_date = new DateTime($val);

        return $this;
    }

    public function setPublicDate(string $val): self
    {
        $this->public_date = new DateTime($val);

        return $this;
    }

    public function setConfidence(int $val): self
    {
        $this->confidence = Confidence::from($val);

        return $this;
    }
}
