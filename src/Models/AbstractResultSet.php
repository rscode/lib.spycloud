<?php

namespace Ratespecial\SpyCloud\Models;

use ArrayAccess;

abstract class AbstractResultSet implements ArrayAccess
{
    /**
     * Token used to paginate through requested items.
     *
     * @var string
     */
    public string $cursor = '';

    /**
     * Total number of objects available as a result of your request.
     *
     * @var int
     */
    public int $hits = 0;

    public array $results = [];


    public function offsetExists(mixed $offset): bool
    {
        return isset($this->results[$offset]);
    }

    public function offsetGet(mixed $offset): mixed
    {
        return $this->results[$offset];
    }

    public function offsetSet(mixed $offset, mixed $value): void
    {
        $this->results[$offset] = $value;
    }

    public function offsetUnset(mixed $offset): void
    {
        unset($this->results[$offset]);
    }
}
