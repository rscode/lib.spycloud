<?php

namespace Ratespecial\SpyCloud\Models\BreachAssets;

use DateTime;
use Exception;

trait PiiAssets
{
    /**
     * Set to 'y' if this person is classified as an active investor, otherwise set to 'n'.
     * @var string
     */
    public string $active_investor = '';

    /**
     * Address line 1.
     * @var string
     */
    public string $address_1 = '';

    /**
     * Address line 2.
     * @var string
     */
    public string $address_2 = '';

    /**
     * Age (in years).
     * @var int
     */
    public int $age = 0;

    /**
     * Age in a numerical range format.
     * @var string
     */
    public string $age_range = '';

    /**
     * Birth location of this person.
     * @var string
     */
    public string $birthplace = '';

    /**
     * Birth year.
     * @var int
     */
    public int $birth_year = 0;

    /**
     * Set to 'y' if this person is classified as having purchased products online, otherwise set to 'n'.
     * @var string
     */
    public string $buys_online = '';

    /**
     * Set to 'y' if this person is classified as a cat owner, otherwise set to 'n'.
     * @var string
     */
    public string $cat_owner = '';

    /**
     * Set to 'y' if this person is classified as being part of a Christian family, otherwise set to 'n'.
     * @var string
     */
    public string $christian_family = '';

    /**
     * City name.
     * @var string
     */
    public string $city = '';

    /**
     * Address associated with company name.
     * @var string
     */
    public string $company_address = '';

    /**
     * Country name.
     * @var string
     */
    public string $country = '';

    /**
     * Country code; derived from country.
     * @var string
     */
    public string $country_code = '';

    /**
     * County name.
     * @var string
     */
    public string $county = '';

    /**
     * Credit rating for this person. See list of possible values available here.
     * @var string
     */
    public string $credit_rating = '';

    /**
     * Date of death of this person.
     * @var DateTime|null
     */
    public ?DateTime $date_of_death = null;

    /**
     * Model of this person's device.
     * @var string
     */
    public string $device_model = '';

    /**
     * Name of this person's device.
     * @var string
     */
    public string $device_name = '';

    /**
     * Date of birth. In ISO 8601 datetime format.
     * @var DateTime|null
     */
    public ?DateTime $dob = null;

    /**
     * Set to 'y' if this person is classified as a dog owner, otherwise set to 'n'.
     * @var string
     */
    public string $dog_owner = '';

    /**
     * Driver's license number.
     * @var string
     */
    public string $drivers_license = '';

    /**
     * State code of driver's license; derived from drivers_license if prepended.
     * @var string
     */
    public string $drivers_license_state_code = '';

    /**
     * First name of emergency contact.
     * @var string
     */
    public string $ec_first_name = '';

    /**
     * Full name of emergency contact.
     * @var string
     */
    public string $ec_full_name = '';

    /**
     * Last name of emergency contact.
     * @var string
     */
    public string $ec_last_name = '';

    /**
     * Phone number of emergency contact.
     * @var string
     */
    public string $ec_phone = '';

    /**
     * Postal code of emergency contact.
     * @var string
     */
    public string $ec_postal_code = '';

    /**
     * Relationship with emergency contact.
     * @var string
     */
    public string $ec_relation = '';

    /**
     * Level of education completed. See list of possible values available here.
     * @var string
     */
    public string $education = '';

    /**
     * Name of the school, training facility, or educational institution.
     * @var string
     */
    public string $educational_institution = '';

    /**
     * Estimated income range. See list of possible values available here.
     * @var string
     */
    public string $estimated_income = '';

    /**
     * Ethnic group associated with this person. See list of possible values available here.
     * @var string
     */
    public string $ethnic_group = '';

    /**
     * Ethnicity of this person. See list of possible values available here.
     * @var string
     */
    public string $ethnicity = '';

    /**
     * Fax number.
     * @var string
     */
    public string $fax = '';

    /**
     * First name.
     * @var string
     */
    public string $first_name = '';

    /**
     * Full name.
     * @var string
     */
    public string $full_name = '';

    /**
     * Gender specifier. Typically set to 'M', 'F', 'Male', or 'Female'.
     * @var string
     */
    public string $gender = '';

    /**
     * Geolocation coordinates. Stored as 'latitude,longitude'.
     * @var string
     */
    public string $geolocation = '';

    /**
     * Set to 'y' if this person is classified as having grandchildren, otherwise set to 'n'.
     * @var string
     */
    public string $grandchildren = '';

    /**
     * Type of air conditioning. See list of possible values available here.
     * @var string
     */
    public string $has_air_conditioning = '';

    /**
     * Set to 'y' if this person is classified as having an American Express credit card, otherwise set to 'n'.
     * @var string
     */
    public string $has_amex_card = '';

    /**
     * Set to 'y' if this person has a credit card, otherwise set to 'n'.
     * @var string
     */
    public string $has_credit_cards = '';

    /**
     * Set to 'y' if this person is classified as having children, otherwise set to 'n'.
     * @var string
     */
    public string $has_children = '';

    /**
     * Set to 'y' if this person is classified as having a Discover credit card, otherwise set to 'n'.
     * @var string
     */
    public string $has_discover_card = '';

    /**
     * Set to 'y' if this person is classified as having a MasterCard credit card, otherwise set to 'n'.
     * @var string
     */
    public string $has_mastercard = '';

    /**
     * Set to 'y' if this person is classified as having pets, otherwise set to 'n'.
     * @var string
     */
    public string $has_pets = '';

    /**
     * Set to 'y' if this person is classified as having a swimming pool, otherwise set to 'n'.
     * @var string
     */
    public string $has_swimming_pool = '';
    /**
     * Set to 'y' if this person is classified as having a VISA credit card, otherwise set to 'n'.
     * @var string
     */
    public string $has_visa_card = '';

    /**
     * List of hobbies and interests associated with this person.
     * See list of possible values available here.
     * @var array
     */
    public array $hobbies_and_interests = [];

    /**
     * User's homepage URL.
     * @var string
     */
    public string $homepage = '';

    /**
     * Home build year.
     * @var string
     */
    public string $home_build_year = '';

    /**
     * Home purchase date.
     * @var string
     */
    public ?DateTime $home_purchase_date = null;

    /**
     * Home purchase price.
     * @var string
     */
    public string $home_purchase_price = '';

    /**
     * Home transaction type. See list of possible values available here.
     * @var string
     */
    public string $home_transaction_type = '';

    /**
     * Current estimated home value.
     * @var string
     */
    public string $home_value = '';

    /**
     * Industry in which this person works.
     * @var string
     */
    public string $industry = '';

    /**
     * Field is for any other specific interests the person has.
     * @var string
     */
    public string $interested_in = '';

    /**
     * Name of internet service provider.
     * @var string
     */
    public string $isp = '';

    /**
     * Set to 'y' if this person is classified as having made personal investments, otherwise set to 'n'.
     * @var string
     */
    public string $investments_personal = '';

    /**
     * Set to 'y' if this person is classified as having made real estate investments, otherwise set to 'n'.
     * @var string
     */
    public string $investments_real_estate = '';

    /**
     * List of one or more IP addresses in alphanumeric format. Both IPV4 and IPv6 addresses are supported.
     * @var array
     */
    public array $ip_addresses = [];

    /**
     * Set to 'y' if this person is classified as a smoker, otherwise set to 'n'.
     * @var string
     */
    public string $is_smoker = '';

    /**
     * Item purchased during a retail transaction.
     * @var string
     */
    public string $item_purchased = '';

    /**
     * Account language preferences.
     * @var string
     */
    public string $language = '';

    /**
     * Last name.
     * @var string
     */
    public string $last_name = '';

    /**
     * Marital status of this person. See list of possible values available here.
     * @var string
     */
    public string $marital_status = '';

    /**
     * Middle name.
     * @var string
     */
    public string $middle_name = '';

    /**
     * Identification number associated with military service.
     * @var string
     */
    public string $military_id = '';

    /**
     * Rank of individual in the military.
     * @var string
     */
    public string $military_rank = '';

    /**
     * Unit in the military.
     * @var string
     */
    public string $military_unit = '';

    /**
     * The mobile network that a cell phone is part of.
     * @var string
     */
    public string $mobile_carrier = '';

    /**
     * 14 or 15 character (without dashes) hexadecimal value that uniquely identifies a specific mobile device.
     * @var string
     */
    public string $mobile_equipment_id = '';

    /**
     * Mortgage amount.
     * @var string
     */
    public string $mortgage_amount = '';

    /**
     * Mortgage lender name.
     * @var string
     */
    public string $mortgage_lender_name = '';

    /**
     * Mortgage loan type. See list of possible values available here.
     * @var string
     */
    public string $mortgage_loan_type = '';

    /**
     * Mortgage rate.
     * @var string
     */
    public string $mortgage_rate = '';

    /**
     * Name suffix.
     * @var string
     */
    public string $name_suffix = '';

    /**
     * National Identification number.
     * @var string
     */
    public string $national_id = '';

    /**
     * Varies by country, but generally refers to a unique identifier assigned by a governmental entity to a citizen or lawful resident of that country.
     * @var string
     */
    public string $national_provider_id = '';

    /**
     * Net worth of this person. See list of possible values available here.
     * @var string
     */
    public string $net_worth = '';

    /**
     * Number of children.
     * @var int
     */
    public int $number_children = 0;

    /**
     * Passport country.
     * @var string
     */
    public string $passport_country = '';

    /**
     * Passport expiration date.
     * @var DateTime|null
     */
    public ?DateTime $passport_exp_date = null;

    /**
     * Passport issue date.
     * @var DateTime|null
     */
    public ?DateTime $passport_issue_date = null;

    /**
     * Passport number.
     * @var string
     */
    public string $passport_number = '';

    /**
     * Payable to name.
     * @var string
     */
    public string $payableto = '';

    /**
     * Phone number.
     * @var string
     */
    public string $phone = '';

    /**
     * Political affiliation of this person. 'R' for Republican, 'D' for Democrat, 'I' for Independent, 'O' for other.
     * @var string
     */
    public string $political_affiliation = '';

    /**
     * Postal code, usually zip code in USA.
     * @var string
     */
    public string $postal_code = '';

    /**
     * Reverse DNS ptr record.
     * @var string
     */
    public string $ptr_records = '';

    /**
     * Real estate proof number or license number.
     * @var string
     */
    public string $real_estate_id = '';

    /**
     * Religion associated with this person. See list of possible values available here.
     * @var string
     */
    public string $religion = '';

    /**
     * Number of years at current residence. This value tops out at 15, so 15 may indicate 15 years or more.
     * @var int
     */
    public int $residence_length_years = 0;

    /**
     * Salary amount.
     * @var string
     */
    public string $salary_amount = '';

    /**
     * Sewer type. See list of possible values available here.
     * @var string
     */
    public string $sewer_type = '';

    /**
     * Set to 'y' if this person is classified as a single parent, otherwise set to 'n'.
     * @var string
     */
    public string $single_parent = '';

    /**
     * SHA1 hash of the social security number.
     * @var string
     */
    public string $social_security_number = '';

    /**
     * The last four digits of the social security number.
     * @var string
     */
    public string $ssn_last_four = '';

    /**
     * State name.
     * @var string
     */
    public string $state = '';

    /**
     * An identifier assigned by a school or educational institution to a student of that institution.
     * @var string
     */
    public string $student_id = '';

    /**
     * Timezone or timezone offset.
     * @var string
     */
    public string $timezone = '';

    /**
     * Title of this person.
     * @var string
     */
    public string $title = '';

    /**
     * Vehicle Identification Number.
     * @var string
     */
    public string $vehicle_identification_number = '';

    /**
     * Vehicle make.
     * @var string
     */
    public string $vehicle_make = '';

    /**
     * Vehicle model.
     * @var string
     */
    public string $vehicle_model = '';

    /**
     * Text that exists on a vehicle’s license plate, which may vary based on local regulations.
     * @var string
     */
    public string $vehicle_plate_number = '';

    /**
     * Vehicle year.
     * @var int
     */
    public int $vehicle_year = 0;

    /**
     * Voter ID.
     * @var string
     */
    public string $voter_id = '';

    /**
     * Voter registration date.
     * @var DateTime|null
     */
    public ?DateTime $voter_registration_date = null;

    /**
     * Water type. See list of possible values available here.
     * @var string
     */
    public string $water_type = '';


    /**
     * @throws Exception
     */
    public function setVoterRegistrationDate(string $val): self
    {
        $this->voter_registration_date = new DateTime($val);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function setPassportIssueDate(string $val): self
    {
        $this->passport_issue_date = new DateTime($val);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function setPassportExpDate(string $val): self
    {
        $this->passport_exp_date = new DateTime($val);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function setHomePurchaseDate(string $val): self
    {
        $this->home_purchase_date = new DateTime($val);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function setDob(string $val): self
    {
        $this->dob = new DateTime($val);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function setDateofDeath(string $val): self
    {
        $this->date_of_death = new DateTime($val);

        return $this;
    }
}
