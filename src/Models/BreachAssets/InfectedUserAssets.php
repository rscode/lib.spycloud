<?php

namespace Ratespecial\SpyCloud\Models\BreachAssets;

use DateTime;
use Exception;

trait InfectedUserAssets
{
    public array $av_softwares = [];
    public string $display_resolution = '';
    public string $form_cookies_data = '';
    public string $form_post_data = '';
    public string $infected_machine_id = '';
    public string $infected_path = '';
    public ?DateTime $infected_time = null;
    public string $keyboard_languages = '';
    public string $log_id = '';
    public string $logon_server = '';
    public string $port = '';
    public string $mac_address = '';
    public ?DateTime $system_install_date = null;
    public string $system_model = '';
    public string $target_domain = '';
    public string $target_subdomain = '';
    public string $target_url = '';
    public string $user_agent = '';
    public string $user_browser = '';
    public string $user_hostname = '';
    public string $user_os = '';
    public string $user_sys_domain = '';
    public string $user_sys_registered_organization = '';
    public string $user_sys_registered_owner = '';

    /**
     * @throws Exception
     */
    public function setInfectedTime(string $val): self
    {
        $this->infected_time = new DateTime($val);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function setSystemInstallDate(string $val): self
    {
        $this->system_install_date = new DateTime($val);

        return $this;
    }
}
