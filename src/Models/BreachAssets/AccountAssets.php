<?php

namespace Ratespecial\SpyCloud\Models\BreachAssets;

use DateTime;
use Exception;

trait AccountAssets
{
    public string $account_caption = '';
    public string $account_id = '';
    public string $account_image_url = '';
    public ?DateTime $account_last_activity_time = null;
    public string $account_login_time = '';
    public string $account_modification_time = '';
    public string $account_nickname = '';
    public string $account_notes = '';
    public string $account_password_date = '';
    public string $account_secret = '';
    public string $account_secret_question = '';
    public string $account_signup_time = '';
    public string $account_status = '';
    public string $account_title = '';
    public string $account_type = '';
    public string $api_token = '';
    public string $api_token_secret = '';
    public string $backup_email = '';
    public string $backup_email_username = '';
    public string $domain = '';
    public string $email = '';
    public string $email_domain = '';
    public string $email_username = '';
    public string $num_posts = '';
    public string $password = '';
    public string $password_plaintext = '';
    public string $password_type = '';
    public string $private_key = '';
    public string $private_key_password = '';
    public string $public_key = '';
    public string $salt = '';
    public string $service = '';
    public string $service_expiration = '';
    public string $username = '';

    /**
     * @throws Exception
     */
    public function setAccountLastActivityTime(string $val): self
    {
        $this->account_last_activity_time = new DateTime($val);

        return $this;
    }
}
