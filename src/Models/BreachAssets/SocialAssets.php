<?php

namespace Ratespecial\SpyCloud\Models\BreachAssets;

trait SocialAssets
{
    public array $social_aim = [];
    public array $social_aboutme = [];
    public array $social_angellist = [];
    public array $social_behance = [];
    public array $social_crunchbase = [];
    public array $social_dribble = [];
    public array $social_facebook = [];
    public array $social_flickr = [];
    public array $social_foursquare = [];
    public array $social_github = [];
    public array $social_gitlab = [];
    public array $social_gravatar = [];
    public array $social_google = [];
    public array $social_icq = [];
    public array $social_indeed = [];
    public array $social_instagram = [];
    public array $social_jabber = [];
    public array $social_klout = [];
    public array $social_line = [];
    public array $social_linkedin = [];
    public array $social_medium = [];
    public array $social_meetup = [];
    public array $social_msn = [];
    public array $social_myspace = [];
    public array $social_other = [];
    public array $social_pinterest = [];
    public array $social_quora = [];
    public array $social_reddit = [];
    public array $social_skype = [];
    public array $social_qq = [];
    public array $social_soundcloud = [];
    public array $social_stackoverflow = [];
    public array $social_steam = [];
    public array $social_telegram = [];
    public array $social_tiktok = [];
    public array $social_twitter = [];
    public array $social_vk = [];
    public array $social_vimeo = [];
    public array $social_wechat = [];
    public array $social_weibo = [];
    public array $social_whatsapp = [];
    public array $social_wordpress = [];
    public array $social_xing = [];
    public array $social_yahoo = [];
    public array $social_youtube = [];
}
