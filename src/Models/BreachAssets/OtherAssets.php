<?php

namespace Ratespecial\SpyCloud\Models\BreachAssets;

use DateTime;
use Exception;

trait OtherAssets
{
    public string $desc = '';
    public string $guid = '';
    public string $pastebin_key = '';
    public ?DateTime $record_addition_date = null;
    public ?DateTime $record_cracked_date = null;
    public ?DateTime $record_modification_date = null;
    public string $source_file = '';

    /**
     * @throws Exception
     */
    public function setRecordAdditionDate(string $val): self
    {
        $this->record_addition_date = new DateTime($val);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function setRecordCrackedDate(string $val): self
    {
        $this->record_cracked_date = new DateTime($val);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function setRecordModificationDate(string $val): self
    {
        $this->record_modification_date = new DateTime($val);

        return $this;
    }
}
