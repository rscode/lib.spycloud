<?php

namespace Ratespecial\SpyCloud\Models\BreachAssets;

use DateTime;
use Exception;

trait CrmAssets
{
    public string $email_status = '';
    public ?DateTime $crm_last_activity = null;
    public ?DateTime $crm_contact_created = null;

    /**
     * @throws Exception
     */
    public function setCrmLastActivity(string $val): self
    {
        $this->crm_last_activity = new DateTime($val);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function setCrmContactCreated(string $val): self
    {
        $this->crm_contact_created = new DateTime($val);

        return $this;
    }
}
