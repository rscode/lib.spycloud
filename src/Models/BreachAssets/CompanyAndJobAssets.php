<?php

namespace Ratespecial\SpyCloud\Models\BreachAssets;

use DateTime;
use Exception;

trait CompanyAndJobAssets
{
    public string $company_name = '';
    public string $company_website = '';
    public string $revenue = '';
    public string $employees = '';
    public string $job_title = '';
    public string $job_level = '';
    public ?DateTime $job_start_date = null;
    public int $linkedin_number_connections = 0;
    public string $naics_code = '';
    public string $sic_code = '';

    /**
     * @throws Exception
     */
    public function setJobStartDate(string $val): self
    {
        $this->job_start_date = new DateTime($val);

        return $this;
    }
}
