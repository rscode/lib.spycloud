<?php

namespace Ratespecial\SpyCloud\Models\BreachAssets;

trait MedicalAssets
{
    public string $dea_registration_number = '';
    public string $health_insurance_id = '';
    public string $health_insurance_provider = '';
}
