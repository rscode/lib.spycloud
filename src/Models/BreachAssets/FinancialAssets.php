<?php

namespace Ratespecial\SpyCloud\Models\BreachAssets;

trait FinancialAssets
{
    public string $bank_name = '';
    public string $bank_number = '';
    public string $bank_routing_number = '';
    public string $cc_bin = '';
    public string $cc_code = '';
    public string $cc_expiration = '';
    public string $cc_gateway = '';
    public string $cc_last_four = '';
    public string $cc_number = '';
    public string $cc_type = '';
    public string $cryptocurrency_addresses = '';
    public string $taxid = '';
}
