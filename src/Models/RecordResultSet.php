<?php

namespace Ratespecial\SpyCloud\Models;

class RecordResultSet extends AbstractResultSet
{
    /**
     * @var BreachRecord[]
     */
    public array $results = [];

    /**
     * Array of document IDs for all results in this set.
     *
     * @return array
     */
    public function getDocumentIds(): array
    {
        return array_reduce($this->results, function ($result, $hit) {
            $result[] = $hit->document_id;

            return $result;
        }, []);
    }

    /**
     * Array of unique IPs in results
     *
     * @return array    ['1.2.3.4', '5.6.7.8']
     */
    public function getUniqueIpAddresses(): array
    {
        $results = array_reduce($this->results, function ($result, $hit) {
            if (!isset($hit->ip_addresses) || !is_array($hit->ip_addresses)) {
                return $result;
            }

            return array_merge($result, $hit->ip_addresses);
        }, []);

        return array_unique($results);
    }

    /**
     * Array of unique emails in results
     *
     * @return array    ['bob@example.org', 'joan@example.org']
     */
    public function getUniqueEmailAddresses(): array
    {
        $results = array_reduce($this->results, function ($result, $hit) {
            if (!empty($hit->email)) {
                $result[] = strtolower($hit->email);
            }

            return $result;
        }, []);

        return array_unique($results);
    }

    /**
     * Array of unique passwords in results
     *
     * @return array    ['pass123', 'joanofark']
     */
    public function getUniquePasswords(): array
    {
        $results = array_reduce($this->results, function ($result, $hit) {
            if (!empty($hit->password_plaintext)) {
                $result[] = $hit->password_plaintext;
            } elseif (!empty($hit->password)) {
                $result[] = $hit->password;
            }

            return $result;
        }, []);

        return array_unique($results);
    }

    public function offsetGet(mixed $offset): BreachRecord
    {
        return $this->results[$offset];
    }
}
