<?php

namespace Ratespecial\SpyCloud\Models;

class CatalogResultSet extends AbstractResultSet
{
    /**
     * @var BreachCatalog[]
     */
    public array $results = [];

    public function offsetGet(mixed $offset): BreachCatalog
    {
        return $this->results[$offset];
    }
}
