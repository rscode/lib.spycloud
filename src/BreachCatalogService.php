<?php

namespace Ratespecial\SpyCloud;

use Carbon\Carbon;
use Generator;
use Ratespecial\SpyCloud\Models\BreachCatalog;
use Ratespecial\SpyCloud\Models\CatalogResultSet;

/**
 * Assists in fetching from the paginated list of breach catalogs.
 */
class BreachCatalogService
{
    public function __construct(protected readonly SpyCloudService $cloudService)
    {
    }

    /**
     * @param string        $query
     * @param Carbon|string $since YYYY-MM-DD format
     * @param Carbon|string $until YYYY-MM-DD format
     * @param int           $pageLimit
     * @return Generator<CatalogResultSet>
     */
    public function getByCatalogSet(
        string $query = '',
        Carbon|string $since = '',
        Carbon|string $until = '',
        int $pageLimit = 0,
    ): Generator {
        $cursor            = '';
        $currentPageNumber = 0;

        do {
            $res = $this->cloudService->getBreachCatalog($cursor, $query, $since, $until);

            if (!$res) {
                break;
            }

            yield $res;

            ++$currentPageNumber;
            $cursor = $res->cursor;
        } while (!empty($cursor) && ($pageLimit === 0 || $currentPageNumber < $pageLimit));
    }

    /**
     * @param string        $query
     * @param Carbon|string $since YYYY-MM-DD format
     * @param Carbon|string $until YYYY-MM-DD format
     * @param int           $pageLimit
     * @return Generator<BreachCatalog>
     */
    public function getByBreachCatalog(
        string $query = '',
        Carbon|string $since = '',
        Carbon|string $until = '',
        int $pageLimit = 0,
    ): Generator {
        foreach ($this->getByCatalogSet($query, $since, $until, $pageLimit) as $catalog) {
            foreach ($catalog->results as $breachRecord) {
                yield $breachRecord;
            }
        }
    }
}