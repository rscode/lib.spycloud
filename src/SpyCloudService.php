<?php

namespace Ratespecial\SpyCloud;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use JsonMapper;
use JsonMapper_Exception;
use Ratespecial\SpyCloud\Enums\DomainRecordType;
use Ratespecial\SpyCloud\Exceptions\InvalidSearchCriteriaException;
use Ratespecial\SpyCloud\Exceptions\InvalidSearchParameterException;
use Ratespecial\SpyCloud\Models\BreachCatalog;
use Ratespecial\SpyCloud\Models\BreachRecord;
use Ratespecial\SpyCloud\Models\CatalogResultSet;
use Ratespecial\SpyCloud\Models\RecordResultSet;

class SpyCloudService
{
    private const SPYCLOUD_URL = 'https://api.spycloud.io/sp-v2/breach';


    public function __construct(protected readonly Client $client, protected string $apiKey)
    {
    }

    public function getBreachById(int $id): ?BreachCatalog
    {
        $url = self::SPYCLOUD_URL . '/catalog/' . $id;

        /** @var CatalogResultSet $resultSet */
        $resultSet = $this->execute($url, [], CatalogResultSet::class);

        if ($resultSet->hits === 0) {
            return null;
        }

        return $resultSet[0];
    }

    /**
     * @param string        $cursor
     * @param string        $query
     * @param Carbon|string $since YYYY-MM-DD format
     * @param Carbon|string $until YYYY-MM-DD format
     * @return CatalogResultSet|null
     * @throws GuzzleException
     * @throws InvalidSearchCriteriaException
     * @throws InvalidSearchParameterException
     * @throws JsonMapper_Exception
     */
    public function getBreachCatalog(
        string $cursor = '',
        string $query = '',
        Carbon|string $since = '',
        Carbon|string $until = '',
    ): ?CatalogResultSet {
        $url = self::SPYCLOUD_URL . '/catalog';

        $queryParams = [];

        if (!empty($cursor)) {
            $queryParams['cursor'] = $cursor;
        }
        if (!empty($query)) {
            $queryParams['query'] = $query;
        }
        if ($since) {
            $queryParams['since'] = $this->carbonToString($since);
        }
        if ($until) {
            $queryParams['until'] = $this->carbonToString($until);
        }

        /** @var CatalogResultSet $resultSet */
        $resultSet = $this->execute($url, $queryParams, CatalogResultSet::class);

        if ($resultSet->hits === 0) {
            return null;
        }

        return $resultSet;
    }

    /**
     * @param string           $domain
     * @param DomainRecordType $type
     * @param string           $cursor
     * @param Carbon|string    $since YYYY-MM-DD format
     * @param Carbon|string    $until YYYY-MM-DD format
     * @param int|array        $severity
     * @return RecordResultSet
     * @throws GuzzleException
     * @throws InvalidSearchCriteriaException
     * @throws InvalidSearchParameterException
     * @throws JsonMapper_Exception
     */
    public function getDomainBreachData(
        string $domain,
        DomainRecordType $type = DomainRecordType::ALL,
        string $cursor = '',
        Carbon|string $since = '',
        Carbon|string $until = '',
        int|array $severity = 0,
    ): RecordResultSet {
        $query = [
            'type'   => $type,
            'cursor' => $cursor,
            'since'  => $this->carbonToString($since),
            'until'  => $this->carbonToString($until),
        ];

        if (is_array($severity)) {
            $query['severity'] = implode(',', $severity);
        } elseif ($severity > 0) {
            $query['severity'] = $severity;
        }

        $url = self::SPYCLOUD_URL . '/data/domains/' . $domain;

        return $this->execute($url, $query, RecordResultSet::class);
    }

    /**
     * @param string|string[] $email Search between 1 and 10 email addresses
     * @param Carbon|string   $since YYYY-MM-DD format
     * @param Carbon|string   $until YYYY-MM-DD format
     * @param int|array       $severity
     * @return RecordResultSet
     * @throws GuzzleException
     * @throws InvalidSearchCriteriaException
     * @throws InvalidSearchParameterException
     * @throws JsonMapper_Exception
     * @see https://spyclouddatapartnershipapiv2.docs.apiary.io/#reference/0/emails/get-email-breach-data
     */
    public function getEmailBreachData(
        string|array $email,
        Carbon|string $since = '',
        Carbon|string $until = '',
        int|array $severity = 0
    ): RecordResultSet {
        $query = [
            'since'  => $this->carbonToString($since),
            'until'  => $this->carbonToString($until),
        ];

        if (is_array($severity)) {
            $query['severity'] = implode(',', $severity);
        } elseif ($severity > 0) {
            $query['severity'] = $severity;
        }

        if (is_array($email)) {
            $email = implode(',', $email);
        }

        $url = self::SPYCLOUD_URL . '/data/emails/' . $email;

        return $this->execute($url, $query, RecordResultSet::class);
    }

    /**
     * @param string        $ip
     * @param string        $cursor
     * @param Carbon|string $since YYYY-MM-DD format
     * @param Carbon|string $until YYYY-MM-DD format
     * @param int|array     $severity
     * @return RecordResultSet
     * @throws GuzzleException
     * @throws InvalidSearchCriteriaException
     * @throws InvalidSearchParameterException
     * @throws JsonMapper_Exception
     */
    public function getIpBreachData(
        string $ip,
        string $cursor = '',
        Carbon|string $since = '',
        Carbon|string $until = '',
        int|array $severity = 0
    ): RecordResultSet {
        $query = [
            'cursor' => $cursor,
            'since'  => $this->carbonToString($since),
            'until'  => $this->carbonToString($until),
        ];

        if (is_array($severity)) {
            $query['severity'] = implode(',', $severity);
        } elseif ($severity > 0) {
            $query['severity'] = $severity;
        }

        $url = self::SPYCLOUD_URL . '/data/ips/' . $ip;

        return $this->execute($url, $query, RecordResultSet::class);
    }

    /**
     * @param string        $username
     * @param string        $cursor
     * @param Carbon|string $since YYYY-MM-DD format
     * @param Carbon|string $until YYYY-MM-DD format
     * @param int|array     $severity
     * @return RecordResultSet
     * @throws GuzzleException
     * @throws InvalidSearchCriteriaException
     * @throws InvalidSearchParameterException
     * @throws JsonMapper_Exception
     */
    public function getUsernameBreachData(
        string $username,
        string $cursor = '',
        Carbon|string $since = '',
        Carbon|string $until = '',
        int|array $severity = 0
    ): RecordResultSet {
        $query = [
            'cursor' => $cursor,
            'since'  => $this->carbonToString($since),
            'until'  => $this->carbonToString($until),
        ];

        if (is_array($severity)) {
            $query['severity'] = implode(',', $severity);
        } elseif ($severity > 0) {
            $query['severity'] = $severity;
        }

        $url = self::SPYCLOUD_URL . '/data/usernames/' . $username;

        return $this->execute($url, $query, RecordResultSet::class);
    }

    /**
     * @param string        $phoneNumber
     * @param string        $cursor
     * @param Carbon|string $since YYYY-MM-DD format
     * @param Carbon|string $until YYYY-MM-DD format
     * @param int|array     $severity
     * @return RecordResultSet
     * @throws GuzzleException
     * @throws InvalidSearchCriteriaException
     * @throws InvalidSearchParameterException
     * @throws JsonMapper_Exception
     */
    public function getPhoneNumberBreachData(
        string $phoneNumber,
        string $cursor = '',
        Carbon|string $since = '',
        Carbon|string $until = '',
        int|array $severity = 0
    ): RecordResultSet {
        $query = [
            'cursor' => $cursor,
            'since'  => $this->carbonToString($since),
            'until'  => $this->carbonToString($until),
        ];

        if (is_array($severity)) {
            $query['severity'] = implode(',', $severity);
        } elseif ($severity > 0) {
            $query['severity'] = $severity;
        }

        $url = self::SPYCLOUD_URL . '/data/phone-numbers/' . $phoneNumber;

        return $this->execute($url, $query, RecordResultSet::class);
    }

    /**
     * @param string        $bankNumber    Can search by plain text, sha1, sha256, or sha512
     * @param string        $cursor
     * @param Carbon|string $since YYYY-MM-DD format
     * @param Carbon|string $until YYYY-MM-DD format
     * @param int|array     $severity
     * @return RecordResultSet
     * @throws GuzzleException
     * @throws InvalidSearchCriteriaException
     * @throws InvalidSearchParameterException
     * @throws JsonMapper_Exception
     */
    public function getBankNumberBreachData(
        string $bankNumber,
        string $cursor = '',
        Carbon|string $since = '',
        Carbon|string $until = '',
        int|array $severity = 0
    ): RecordResultSet {
        $query = [
            'cursor' => $cursor,
            'since'  => $this->carbonToString($since),
            'until'  => $this->carbonToString($until),
        ];

        if (is_array($severity)) {
            $query['severity'] = implode(',', $severity);
        } elseif ($severity > 0) {
            $query['severity'] = $severity;
        }

        $url = self::SPYCLOUD_URL . '/data/bank-numbers/' . $bankNumber;

        return $this->execute($url, $query, RecordResultSet::class);
    }


    /**
     * @param string        $ccn    Can search by plain text, sha1, sha256, or sha512
     * @param string        $cursor
     * @param Carbon|string $since YYYY-MM-DD format
     * @param Carbon|string $until YYYY-MM-DD format
     * @param int|array     $severity
     * @return RecordResultSet
     * @throws GuzzleException
     * @throws InvalidSearchCriteriaException
     * @throws InvalidSearchParameterException
     * @throws JsonMapper_Exception
     */
    public function getCreditCardBreachData(
        string $ccn,
        string $cursor = '',
        Carbon|string $since = '',
        Carbon|string $until = '',
        int|array $severity = 0
    ): RecordResultSet {
        $query = [
            'cursor' => $cursor,
            'since'  => $this->carbonToString($since),
            'until'  => $this->carbonToString($until),
        ];

        if (is_array($severity)) {
            $query['severity'] = implode(',', $severity);
        } elseif ($severity > 0) {
            $query['severity'] = $severity;
        }

        $url = self::SPYCLOUD_URL . '/data/cc-numbers/' . $ccn;

        return $this->execute($url, $query, RecordResultSet::class);
    }

    /**
     * @param string        $driversLicenseNumber   Can search by plain text, sha1, sha256, or sha512
     * @param string        $cursor
     * @param Carbon|string $since YYYY-MM-DD format
     * @param Carbon|string $until YYYY-MM-DD format
     * @param int|array     $severity
     * @return RecordResultSet
     * @throws GuzzleException
     * @throws InvalidSearchCriteriaException
     * @throws InvalidSearchParameterException
     * @throws JsonMapper_Exception
     */
    public function getDriversLicenseBreachData(
        string $driversLicenseNumber,
        string $cursor = '',
        Carbon|string $since = '',
        Carbon|string $until = '',
        int|array $severity = 0
    ): RecordResultSet {
        $query = [
            'cursor' => $cursor,
            'since'  => $this->carbonToString($since),
            'until'  => $this->carbonToString($until),
        ];

        if (is_array($severity)) {
            $query['severity'] = implode(',', $severity);
        } elseif ($severity > 0) {
            $query['severity'] = $severity;
        }

        $url = self::SPYCLOUD_URL . '/data/drivers-licenses/' . $driversLicenseNumber;

        return $this->execute($url, $query, RecordResultSet::class);
    }


    /**
     * @param string        $nid Can search by plain text, sha1, sha256, or sha512
     * @param string        $cursor
     * @param Carbon|string $since YYYY-MM-DD format
     * @param Carbon|string $until YYYY-MM-DD format
     * @param int|array     $severity
     * @return RecordResultSet
     * @throws GuzzleException
     * @throws InvalidSearchCriteriaException
     * @throws InvalidSearchParameterException
     * @throws JsonMapper_Exception
     */
    public function getNationalIdBreachData(
        string $nid,
        string $cursor = '',
        Carbon|string $since = '',
        Carbon|string $until = '',
        int|array $severity = 0
    ): RecordResultSet {
        $query = [
            'cursor' => $cursor,
            'since'  => $this->carbonToString($since),
            'until'  => $this->carbonToString($until),
        ];

        if (is_array($severity)) {
            $query['severity'] = implode(',', $severity);
        } elseif ($severity > 0) {
            $query['severity'] = $severity;
        }

        $url = self::SPYCLOUD_URL . '/data/national-ids/' . $nid;

        return $this->execute($url, $query, RecordResultSet::class);
    }


    /**
     * @param string        $ppn Can search by plain text, sha1, sha256, or sha512
     * @param string        $cursor
     * @param Carbon|string $since YYYY-MM-DD format
     * @param Carbon|string $until YYYY-MM-DD format
     * @param int|array     $severity
     * @return RecordResultSet
     * @throws GuzzleException
     * @throws InvalidSearchCriteriaException
     * @throws InvalidSearchParameterException
     * @throws JsonMapper_Exception
     */
    public function getPassportBreachData(
        string $ppn,
        string $cursor = '',
        Carbon|string $since = '',
        Carbon|string $until = '',
        int|array $severity = 0
    ): RecordResultSet {
        $query = [
            'cursor' => $cursor,
            'since'  => $this->carbonToString($since),
            'until'  => $this->carbonToString($until),
        ];

        if (is_array($severity)) {
            $query['severity'] = implode(',', $severity);
        } elseif ($severity > 0) {
            $query['severity'] = $severity;
        }

        $url = self::SPYCLOUD_URL . '/data/passport-numbers/' . $ppn;

        return $this->execute($url, $query, RecordResultSet::class);
    }


    /**
     * Note: Hashed input of the SHA-1 must be created from 9 digits without spaces, dashes, or special characters. If
     * using SHA-256/512, you must first take a SHA-1 hash of the plaintext social security number.
     *
     * @param string        $ssn    Can search by sha1, sha256, or sha512
     * @param string        $cursor
     * @param Carbon|string $since YYYY-MM-DD format
     * @param Carbon|string $until YYYY-MM-DD format
     * @param int|array     $severity
     * @return RecordResultSet
     * @throws GuzzleException
     * @throws InvalidSearchCriteriaException
     * @throws InvalidSearchParameterException
     * @throws JsonMapper_Exception
     */
    public function getSsnBreachData(
        string $ssn,
        string $cursor = '',
        Carbon|string $since = '',
        Carbon|string $until = '',
        int|array $severity = 0
    ): RecordResultSet {
        $query = [
            'cursor' => $cursor,
            'since'  => $this->carbonToString($since),
            'until'  => $this->carbonToString($until),
        ];

        if (is_array($severity)) {
            $query['severity'] = implode(',', $severity);
        } elseif ($severity > 0) {
            $query['severity'] = $severity;
        }

        $url = self::SPYCLOUD_URL . '/data/social-security-numbers/' . $ssn;

        return $this->execute($url, $query, RecordResultSet::class);
    }


    /**
     * @throws JsonMapper_Exception
     * @throws InvalidSearchCriteriaException
     * @throws GuzzleException
     * @throws InvalidSearchParameterException
     */
    protected function execute(string $url, array $query, string $resultClass): RecordResultSet|CatalogResultSet
    {
        try {
            $resp = $this->client->get($url, [
                RequestOptions::QUERY   => $query,
                RequestOptions::HEADERS => [
                    'X-Api-Key' => $this->apiKey,
                    'Accept'    => 'application/json',
                ],
            ]);
        } catch (ClientException $ex) {
            if ($ex->hasResponse()) {
                $data = json_decode((string)$ex->getResponse()->getBody());

                $errorMessage = $data->errorMessage ?? '';
                if (preg_match("/Invalid parameter: '(phone_number|email|ip|username)/", $errorMessage)) {
                    throw new InvalidSearchCriteriaException($errorMessage, $ex->getCode(), $ex);
                } elseif (str_contains($errorMessage, 'Invalid parameter')) {
                    throw new InvalidSearchParameterException($errorMessage, $ex->getCode(), $ex);
                }

                throw $ex;
            }
        }

        $data = json_decode((string)$resp->getBody());

        $mapper = new JsonMapper();

        // If there are some new fields in the record that our class doesn't define, set them in the object.
        $mapper->undefinedPropertyHandler = function ($object, $propName, $jsonValue) {
            $object->$propName = $jsonValue;
        };

        return $mapper->map($data, new $resultClass());
    }

    /**
     * Remove duplicate documents from an array of BreachRecords
     *
     * @param BreachRecord[] $hits
     * @return BreachRecord[]
     */
    public function dedupeHits(array $hits): array
    {
        $results = [];

        foreach ($hits as $hit) {
            $results[$hit->document_id] = $hit;
        }

        return array_values($results);
    }

    private function carbonToString(string|Carbon $date): string
    {
        if ($date instanceof Carbon) {
            return $date->toDateString();
        }

        return $date;
    }
}
