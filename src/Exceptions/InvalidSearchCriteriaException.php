<?php

namespace Ratespecial\SpyCloud\Exceptions;

/**
 * What you searched for was invalid.  Probably don't want to do it again.
 */
class InvalidSearchCriteriaException extends InvalidSearchParameterException
{

}