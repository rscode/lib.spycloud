<?php

namespace Ratespecial\SpyCloud\Exceptions;

use Exception;

/**
 * A query parameter like since, until, severity, etc is invalid.
 */
class InvalidSearchParameterException extends Exception
{
}