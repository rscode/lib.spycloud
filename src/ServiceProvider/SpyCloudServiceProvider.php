<?php

namespace Ratespecial\SpyCloud\ServiceProvider;

use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;
use Ratespecial\SpyCloud\SpyCloudService;

/**
 * Laravel service provider.  Auto discovered
 */
class SpyCloudServiceProvider extends ServiceProvider
{
    public function boot()
    {
        // Check for existence of function.  Lumen doesn't have it.
        if (function_exists('config_path')) {
            $this->publishes([
                __DIR__ . '/../../config/spycloud.php' => config_path('spycloud.php'),
            ]);
        }
    }

    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/spycloud.php', 'spycloud');

        $this->registerSpyCloudService();
    }

    protected function registerSpyCloudService(): void
    {
        $this->app->bind(SpyCloudService::class, function () {
            $c   = new Client();
            $key = (string)$this->app['config']['spycloud.api_key'];

            return new SpyCloudService($c, $key);
        });
    }

    public function provides(): array
    {
        return [SpyCloudService::class];
    }
}
