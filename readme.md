# SpyCloud PHP SDK

## Contents

* [Installation](#installation)
* [Usage](#usage)
    * [Laravel usage](#laravel-usage)
    * [Non-Laravel usage](#non-laravel-usage)
* [Getting the most info](#getting-the-most-info)
    * [Using multiple search criteria](#using-multiple-search-criteria)
    * [Getting breach source info](#getting-breach-source-info)
    * [Grabbing entire source catalog](#grabbing-entire-source-catalog)
* [Documentation](#documentation)

## Installation

```
composer require ratespecial/spycloud
```

If you are using Laravel, `SpyCloudServiceProvider` will be auto discovered.

## Usage

### Laravel usage

You must set your SpyCloud API key in your environment.

```dotenv
SPYCLOUD_API_KEY=myspykey
```

Inject `SpyCloudService` in your application. It has functions to access SpyCloud's API end points.

```php
use Ratespecial\SpyCloud\SpyCloudService;

class CustomerScan
{
    public function __construct(protected readonly SpyCloudService $spyCloudService)
    {
    }
    
    public function searchEmail(string $email)
    {
        $result = $this->spyCloudService->getEmailBreachData($email);
        
        // process results
    }
}
```

### Non-Laravel usage

You'll need to configure `SpyCloudService` yourself. It requires a Guzzle client and your API key.

```php
$client   = new \GuzzleHttp\Client();
$apikey   = 'myspykey';
$spycloud = new \Ratespecial\SpyCloud\SpyCloudService($client, $apikey);

$result = $this->spyCloudService->getEmailBreachData('bob@example.org');
```

## Getting the most info

### Using multiple search criteria

Say there are many breach records on a customer. Some have email address, some have usernames, some have phone numbers.
Some might have both email and phone.

To get the most information on a particular user, you should collect each criteria from the customer and hit each search.

1. Email
2. IP
3. Username
4. Phone

Since some records could have multiple data points in them (both email and phone for instance), you can get the same breach
record back from multiple calls. Once you've collected all the breach records, you should unique them using the `document_id`
field. This is the unique ID of the breach record.

`SpyCloudService` has helper functions for this purpose.

```php
$service  = new \Ratespecial\SpyCloud\SpyCloudService();
$set1     = $service->getEmailBreachData('bob_sacamento@example.org');
$set2     = $service->getPhoneNumberBreachData('8188188818');
$combined = array_merge($set1->results, $set2->results);
$finalSet = $service->dedupeHits($combined);
```

### Getting breach source info

Knowing that your email and password are leaked online is a good start. Knowing how it got there is even better.

A key field is `BreachRecord::severity`. A value of `Severity::CRITICAL` means the info was discovered from a
malware infected machine.

Each breach record references a **source**. This is a catalog of records that SpyCloud discovered and processed. This could be
either from a data dump on the dark web, or from a malware infection on a computer.

To find more info on a breach source, take the `source_id` from a `BreachRecord` and call `SpyCloudService::getBreachById()`, which
will give you a `BreachCatalog`.

### Grabbing entire source catalog

The `BreachCatalogService` is a good way to iterate through pages of breach catalog and collect relevant data.  This service decorates 
`SpyCloudService` to support reading pages data.  Be aware this will perform multiple queries to SpyCloud. 

```php
$breach = new \Ratespecial\SpyCloud\BreachCatalogService();

/** @var BreachCatalog $catalog */
foreach ($breach->getByBreachCatalog() as $catalog) {
    // Store catalog data locally
}
```

## Documentation

See [result-schema.json](docs/result-schema.json) for more information on the fields SpyCloud returns on a hit. This is pulled directly
from their API documentation.

https://spyclouddatapartnershipapiv2.docs.apiary.io/#

https://spyclouddataschema.docs.apiary.io/#reference

https://spycloud.com
