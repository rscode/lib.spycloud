<?php

namespace Tests\Ratespecial\SpyCloud;

use JsonMapper;
use PHPUnit\Framework\TestCase;
use Ratespecial\SpyCloud\BreachCatalogService;
use Ratespecial\SpyCloud\Models\BreachCatalog;
use Ratespecial\SpyCloud\Models\CatalogResultSet;
use Ratespecial\SpyCloud\SpyCloudService;

class BreachCatalogServiceTest extends TestCase
{
    private JsonMapper $mapper;

    protected function setUp(): void
    {
        parent::setUp();

        $this->mapper = new JsonMapper();

        // If there are some new fields in the record that our class doesn't define, set them in the object.
        $this->mapper->undefinedPropertyHandler = function ($object, $propName, $jsonValue) {
            $object->$propName = $jsonValue;
        };
    }

    public function testGetByCatalogSet()
    {
        $mCloud = $this->getMockBuilder(SpyCloudService::class)->disableOriginalConstructor()->getMock();
        $mCloud->method('getBreachCatalog')
            ->will($this->onConsecutiveCalls(
                $this->createCatalogSetWithNextPage(),
                $this->createCatalogSetWithNextPage(),
                $this->createCatalogSetWithoutNextPage(),
            ));

        $cnt = 0;
        $s = new BreachCatalogService($mCloud);
        foreach ($s->getByCatalogSet() as $set) {
            ++$cnt;
            $this->assertInstanceOf(CatalogResultSet::class, $set);
        }

        $this->assertSame(3, $cnt);
    }

    public function testGetByBreachCatalog()
    {
        $mCloud = $this->getMockBuilder(SpyCloudService::class)->disableOriginalConstructor()->getMock();
        $mCloud->method('getBreachCatalog')
            ->will($this->onConsecutiveCalls(
                $this->createCatalogSetWithNextPage(),
                $this->createCatalogSetWithNextPage(),
                $this->createCatalogSetWithoutNextPage(),
            ));

        $cnt = 0;
        $s = new BreachCatalogService($mCloud);
        foreach ($s->getByBreachCatalog() as $set) {
            ++$cnt;
            $this->assertInstanceOf(BreachCatalog::class, $set);
        }

        $this->assertSame(3, $cnt);
    }

    private function createCatalogSetWithNextPage(): CatalogResultSet
    {
        $data = json_decode(file_get_contents(TEST_ROOT . '/fixtures/breach-catalog.json'));
        /** @var CatalogResultSet $set */
        $set = $this->mapper->map($data, new CatalogResultSet());
        $set->cursor = 'abc123';
        return $set;
    }

    private function createCatalogSetWithoutNextPage(): CatalogResultSet
    {
        $data = json_decode(file_get_contents(TEST_ROOT . '/fixtures/breach-catalog.json'));
        /** @var CatalogResultSet $set */
        $set = $this->mapper->map($data, new CatalogResultSet());
        return $set;
    }
}
