<?php

namespace Tests\Ratespecial\SpyCloud;

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use Ratespecial\SpyCloud\SpyCloudService;

class LiveTest extends TestCase
{
    public function testBreachById()
    {
        $c = new Client();
        $s = new SpyCloudService($c, getenv('SPYCLOUD_API_KEY'));

        $res = $s->getBreachById(982311923);

        var_dump($res);
    }

    public function testDomainBreachData()
    {
        $c = new Client();
        $s = new SpyCloudService($c, getenv('SPYCLOUD_API_KEY'));

        $res = $s->getDomainBreachData('example.org');

        var_dump($res);
    }

    public function testEmailBreachData()
    {
        $c = new Client();
        $s = new SpyCloudService($c, getenv('SPYCLOUD_API_KEY'));

        $res = $s->getEmailBreachData('zbashirian@example.org');

        var_dump($res);
    }

    public function testUsernameBreachData()
    {
        $c = new Client();
        $s = new SpyCloudService($c, getenv('SPYCLOUD_API_KEY'));

        $res = $s->getUsernameBreachData('captain');

        var_dump($res);
    }

    public function testIpBreachData()
    {
        $c = new Client();
        $s = new SpyCloudService($c, getenv('SPYCLOUD_API_KEY'));

        $res = $s->getIpBreachData('1.2.3.4');

        var_dump($res);
    }

    public function testPhoneNumberBreachData()
    {
        $c = new Client();
        $s = new SpyCloudService($c, getenv('SPYCLOUD_API_KEY'));

        $res = $s->getPhoneNumberBreachData('8188188818');

        var_dump($res);
    }

    public function testBankNumberBreachData()
    {
        $c = new Client();
        $s = new SpyCloudService($c, getenv('SPYCLOUD_API_KEY'));

        // Fictitious test values may return excessive results
        $accountNumber = '123456789';

        $res = $s->getBankNumberBreachData($accountNumber);

        var_dump($res);
    }

    public function testCreditCardBreachData()
    {
        $c = new Client();
        $s = new SpyCloudService($c, getenv('SPYCLOUD_API_KEY'));

        $ccn = '4444333322221111';

        // Can optionally search by SHA1, SHA256, SHA512 of CCN
        $res = $s->getCreditCardBreachData(sha1($ccn));

        var_dump($res);
    }

    public function testDriversLicenseBreachData()
    {
        $c = new Client();
        $s = new SpyCloudService($c, getenv('SPYCLOUD_API_KEY'));

        $dln = 'A1234567';

        // Can optionally search by SHA1, SHA256, SHA512
        $res = $s->getDriversLicenseBreachData(sha1($dln));

        var_dump($res);
    }

    public function testNationalIdBreachData()
    {
        $c = new Client();
        $s = new SpyCloudService($c, getenv('SPYCLOUD_API_KEY'));

        $nid = '1234567890';

        // Can optionally search by SHA1, SHA256, SHA512
        $res = $s->getNationalIdBreachData($nid);

        var_dump($res);
    }

    public function testPassportBreachDataSha1()
    {
        $c = new Client();
        $s = new SpyCloudService($c, getenv('SPYCLOUD_API_KEY'));

        $ppn = '012345678';

        $res = $s->getPassportBreachData($ppn);

        var_dump($res);
    }

    public function testSsnBreachData()
    {
        $c = new Client();
        $s = new SpyCloudService($c, getenv('SPYCLOUD_API_KEY'));

        $ssn = '123456789';

        $res = $s->getSsnBreachData(sha1($ssn));

        var_dump($res);
    }

    public function testBulkBreachCatalog()
    {
        $c = new Client();
        $s = new SpyCloudService($c, getenv('SPYCLOUD_API_KEY'));

        // Results can be up to 1000 per request, so we'll just dump JSON to file as an example
        $ouputFile = fopen("testfile.txt", "w");

        $cursor = '';
        $query  = '';

        $limit = 1;
        $count = 0;

        do {
            $res = $s->getBreachCatalog($cursor, $query, '2023-06-01');

            if ($res) {
                printf('Found %d records', $res->hits);
                echo "\n";

                foreach ($res->results as $breachCatalog) {
                    $breachJson = json_encode($breachCatalog, JSON_PRETTY_PRINT) . "\n";
                    fwrite($ouputFile, $breachJson);
                }

                $count++;
                $cursor = $res->cursor;
                if (!empty($cursor)) {
                    printf('Next cursor: %s', $cursor);
                    echo "\n";
                }
            }

        } while (!empty($cursor) && $count < $limit);

        fclose($ouputFile);
    }
}
