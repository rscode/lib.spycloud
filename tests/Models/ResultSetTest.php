<?php

namespace Tests\Ratespecial\SpyCloud\Models;

use JsonMapper;
use PHPUnit\Framework\TestCase;
use Ratespecial\SpyCloud\Models\RecordResultSet;

class ResultSetTest extends TestCase
{
    public function testGetDocumentIds()
    {
        $set = $this->buildSet();

        $results  = $set->getDocumentIds();
        $expected = [
            '11111111-1111-1111-1111-111111111111',
            '11111111-1111-1111-1111-111111111112',
            '11111111-1111-1111-1111-111111111113',
        ];

        $this->assertEquals($expected, $results);
    }

    public function testGetUniqueIps()
    {
        $set = $this->buildSet();

        $results  = $set->getUniqueIpAddresses();
        $expected = ['1.2.3.4', '4.4.4.4', '6.6.6.6', '7.7.7.7'];

        $this->assertEquals($expected, $results);
    }

    public function testGetUniqueEmails()
    {
        $set = $this->buildSet();

        $results  = $set->getUniqueEmailAddresses();
        $expected = ['bob@example.org', 'james@example.org'];

        $this->assertEquals($expected, $results);
    }

    public function testGetUniquePasswords()
    {
        $set = $this->buildSet();

        $results  = $set->getUniquePasswords();
        $expected = ['12345', 'password'];

        $this->assertEquals($expected, $results);
    }

    private function buildSet(): RecordResultSet
    {
        $data   = json_decode(file_get_contents(TEST_ROOT . '/fixtures/small-result-set.json'));
        $mapper = new JsonMapper();

        return $mapper->map($data, new RecordResultSet());
    }
}
