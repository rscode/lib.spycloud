<?php

namespace Tests\Ratespecial\SpyCloud;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use JsonMapper;
use PHPUnit\Framework\TestCase;
use Ratespecial\SpyCloud\Exceptions\InvalidSearchCriteriaException;
use Ratespecial\SpyCloud\Exceptions\InvalidSearchParameterException;
use Ratespecial\SpyCloud\Models\RecordResultSet;
use Ratespecial\SpyCloud\SpyCloudService;

class SpyCloudServiceTest extends TestCase
{
    public function testSuccessfulEmailBreachData()
    {
        $mock = new MockHandler([
            new Response(body: file_get_contents(__DIR__ . '/fixtures/email-results-success.json')),
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client       = new Client(['handler' => $handlerStack]);

        $s = new SpyCloudService($client, '');

        $result = $s->getEmailBreachData('a@b.com');

        $this->assertSame("arabic (saudi arabia) | english (united states) |", $result[0]->keyboard_languages);
    }

    public function testSuccessfulBreachById()
    {
        $mock = new MockHandler([
            new Response(body: file_get_contents(__DIR__ . '/fixtures/breach-catalog.json')),
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client       = new Client(['handler' => $handlerStack]);

        $s = new SpyCloudService($client, '');

        $result = $s->getBreachById(42);

        $this->assertNull($result->public_date);
        $this->assertCount(4, $result->assets);
    }

    public function testGetByBreachIdMiss()
    {
        $mock = new MockHandler([
            new Response(body: file_get_contents(__DIR__ . '/fixtures/breach-catalog-miss.json')),
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client       = new Client(['handler' => $handlerStack]);

        $s = new SpyCloudService($client, '');

        $result = $s->getBreachById(88888888);

        $this->assertNull($result);
    }

    public function testDedupeHits()
    {
        $set1 = json_decode(file_get_contents(__DIR__ . '/fixtures/small-result-set.json'));
        $set2 = json_decode(file_get_contents(__DIR__ . '/fixtures/small-result-set.json'));

        $combined = array_merge($set1->results, $set2->results);

        $this->assertCount(6, $combined);

        $s = new SpyCloudService(new Client(), '');

        $result = $s->dedupeHits($combined);

        $this->assertCount(3, $result);
    }


    public function testFilterFat()
    {
        $set1 = json_decode(file_get_contents(__DIR__ . '/fixtures/small-result-set.json'));

        $mapper = new JsonMapper();

        // If there are some new fields in the record that our class doesn't define, set them in the object.
        $mapper->undefinedPropertyHandler = function ($object, $propName, $jsonValue) {
            $object->$propName = $jsonValue;
        };

        $resultSet = $mapper->map($set1, new RecordResultSet());

        $breachRecords = [];

        foreach ($resultSet->results as $br) {
            $breachRecords[] = $br->toArray();
        }

        $this->assertTrue(isset($breachRecords[0]['salt']));
        $this->assertFalse(isset($breachRecords[1]['salt']));
    }

    public function testBreachCatalogSuccess()
    {
        $mock = new MockHandler([
            new Response(body: file_get_contents(__DIR__ . '/fixtures/breach-catalog.json')),
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client       = new Client(['handler' => $handlerStack]);

        $s = new SpyCloudService($client, '');

        $result = $s->getBreachCatalog();

        $this->assertSame(1, $result->hits);
        $this->assertCount(1, $result->results);
    }

    public function testInvalidSearchCriteraExceptionIsThrown()
    {
        $mock = new MockHandler([
            new Response(400, body: file_get_contents(__DIR__ . '/fixtures/invalid-email.json')),
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client       = new Client(['handler' => $handlerStack]);

        $s = new SpyCloudService($client, '');

        $this->expectException(InvalidSearchCriteriaException::class);
        $this->expectExceptionMessage("[BadRequest] Invalid parameter: 'email'. Must be one or more comma delimited email addresses or SHA1/SHA256/SHA512 hashes of email addresses.");

        $s->getEmailBreachData('blah');
    }

    public function testInvalidSearchParameterExceptionIsThrown()
    {
        $mock = new MockHandler([
            new Response(400, body: file_get_contents(__DIR__ . '/fixtures/invalid-since.json')),
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client       = new Client(['handler' => $handlerStack]);

        $s = new SpyCloudService($client, '');

        $this->expectException(InvalidSearchParameterException::class);
        $this->expectExceptionMessage("[BadRequest] Invalid parameter: 'since'. ");

        $s->getEmailBreachData('blah');
    }
}
